package com.nbsaas.life.ad.api.domain.request;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import com.nbsaas.boot.rest.request.RequestId;
/**
* 请求对象
*/
@Data
public class AdDataRequest implements Serializable,RequestId {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



        /**
        * 
        **/
            private String note;

        /**
        * 
        **/
            private Date endDate;

        /**
        * 
        **/
            private Integer catalog;

        /**
        * 
        **/
            private String title;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 
        **/
            private String url;

        /**
        * 
        **/
            private Long bussId;

        /**
        * 
        **/
            private Long adPosition;

        /**
        * 
        **/
            private String path;

        /**
        * 
        **/
            private Date beginDate;

        /**
        * 排序号
        **/
            private Integer sortNum;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 
        **/
            //private String adPositionNameName;

        /**
        * 最新修改时间
        **/
            private Date lastDate;
}