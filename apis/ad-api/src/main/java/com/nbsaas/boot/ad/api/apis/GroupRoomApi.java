package com.nbsaas.boot.ad.api.apis;

import com.nbsaas.boot.ad.api.domain.request.GroupRoomDataRequest;
import com.nbsaas.boot.ad.api.domain.simple.GroupRoomSimple;
import com.nbsaas.boot.ad.api.domain.response.GroupRoomResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface GroupRoomApi extends BaseApi<GroupRoomResponse, GroupRoomSimple, GroupRoomDataRequest> {


}
