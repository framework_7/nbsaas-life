package com.nbsaas.boot.ad.api.domain.field;


/**
*   字段映射类
*/
public class GroupRoomField  {



    public static final String  name = "name";


    public static final String  lastNote = "lastNote";


    public static final String  lastMemberName = "lastMemberName";


    public static final String  lastMember = "lastMember";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}