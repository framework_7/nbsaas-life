package com.nbsaas.boot.ad.api.domain.field;


/**
*   字段映射类
*/
public class GroupMessageField  {



    public static final String  note = "note";


    public static final String  name = "name";


    public static final String  member = "member";


    public static final String  memberName = "memberName";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}