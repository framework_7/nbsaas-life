package com.nbsaas.boot.ad.api.domain.field;


/**
*   字段映射类
*/
public class GroupMemberField  {



    public static final String  groupRoom = "groupRoom";


    public static final String  groupRoomName = "groupRoomName";


    public static final String  member = "member";


    public static final String  memberName = "memberName";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  lastDate = "lastDate";

}