package com.nbsaas.life.user.api.domain.field;


/**
*   字段映射类
*/
public class UserOauthTokenField  {



    public static final String  access_token = "access_token";


    public static final String  refresh_token = "refresh_token";


    public static final String  openId = "openId";


    public static final String  id = "id";


    public static final String  tokenType = "tokenType";


    public static final String  user = "user";


    public static final String  addDate = "addDate";


    public static final String  expiresTime = "expiresTime";


    public static final String  loginSize = "loginSize";


    public static final String  lastDate = "lastDate";

}