package com.nbsaas.life.user.api.domain.field;


/**
*   字段映射类
*/
public class UserInfoField  {



    public static final String  phone = "phone";


    public static final String  catalog = "catalog";


    public static final String  name = "name";


    public static final String  storeState = "storeState";


    public static final String  avatar = "avatar";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  loginSize = "loginSize";


    public static final String  lastDate = "lastDate";

}