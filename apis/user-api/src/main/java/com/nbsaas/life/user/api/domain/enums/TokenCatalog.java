package com.nbsaas.life.user.api.domain.enums;

/**
 * 令牌类型
 *
 * @author ada
 */
public enum TokenCatalog {

    /**
     *
     */
  app, others
}
