package com.nbsaas.life.area.api.apis;

import com.nbsaas.life.area.api.domain.request.AreaHotDataRequest;
import com.nbsaas.life.area.api.domain.simple.AreaHotSimple;
import com.nbsaas.life.area.api.domain.response.AreaHotResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface AreaHotApi extends BaseApi<AreaHotResponse, AreaHotSimple, AreaHotDataRequest> {


}
