package com.nbsaas.life.area.api.apis;

import com.nbsaas.life.area.api.domain.request.AreaDataRequest;
import com.nbsaas.life.area.api.domain.simple.AreaSimple;
import com.nbsaas.life.area.api.domain.response.AreaResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface AreaApi extends BaseApi<AreaResponse, AreaSimple, AreaDataRequest> {


}
