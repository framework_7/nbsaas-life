package com.nbsaas.life.system.api.apis;

import com.nbsaas.life.system.api.domain.request.RoleMenuDataRequest;
import com.nbsaas.life.system.api.domain.simple.RoleMenuSimple;
import com.nbsaas.life.system.api.domain.response.RoleMenuResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface RoleMenuApi extends BaseApi<RoleMenuResponse, RoleMenuSimple, RoleMenuDataRequest> {


}
