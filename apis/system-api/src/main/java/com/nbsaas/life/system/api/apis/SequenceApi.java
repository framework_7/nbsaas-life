package com.nbsaas.life.system.api.apis;

import com.nbsaas.life.system.api.domain.request.SequenceDataRequest;
import com.nbsaas.life.system.api.domain.simple.SequenceSimple;
import com.nbsaas.life.system.api.domain.response.SequenceResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface SequenceApi extends BaseApi<SequenceResponse, SequenceSimple, SequenceDataRequest> {


}
