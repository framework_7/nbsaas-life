package com.nbsaas.life.system.api.apis;

import com.nbsaas.boot.rest.api.BaseApi;
import com.nbsaas.life.system.api.domain.request.DictDataRequest;
import com.nbsaas.life.system.api.domain.response.DictResponse;
import com.nbsaas.life.system.api.domain.simple.DictSimple;


/**
 * 对外接口
 */
public interface DictApi extends BaseApi<DictResponse, DictSimple, DictDataRequest> {


}
