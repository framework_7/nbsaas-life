package com.nbsaas.life.system.api.apis;

import com.nbsaas.life.system.api.domain.request.RoleDataRequest;
import com.nbsaas.life.system.api.domain.simple.RoleSimple;
import com.nbsaas.life.system.api.domain.response.RoleResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface RoleApi extends BaseApi<RoleResponse, RoleSimple, RoleDataRequest> {


}
