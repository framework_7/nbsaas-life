package com.nbsaas.life.shop.api.domain.field;


/**
*   字段映射类
*/
public class ShopReviewField  {



    public static final String  parking = "parking";


    public static final String  attire = "attire";


    public static final String  takeout = "takeout";


    public static final String  priceScore = "priceScore";


    public static final String  outdoorSeating = "outdoorSeating";


    public static final String  businessAcceptsCreditCards = "businessAcceptsCreditCards";


    public static final String  serviceScore = "serviceScore";


    public static final String  bikeParking = "bikeParking";


    public static final String  goodForGroups = "goodForGroups";


    public static final String  id = "id";


    public static final String  goodForKids = "goodForKids";


    public static final String  lastDate = "lastDate";


    public static final String  alcohol = "alcohol";


    public static final String  delivery = "delivery";


    public static final String  wifi = "wifi";


    public static final String  caters = "caters";


    public static final String  addDate = "addDate";


    public static final String  noiseLevel = "noiseLevel";


    public static final String  tableService = "tableService";


    public static final String  goodForMealBreakfast = "goodForMealBreakfast";


    public static final String  vote1 = "vote1";


    public static final String  reservations = "reservations";


    public static final String  ambience = "ambience";


    public static final String  vote3 = "vote3";


    public static final String  comment = "comment";


    public static final String  vote2 = "vote2";


    public static final String  hasTV = "hasTV";

}