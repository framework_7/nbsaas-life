package com.nbsaas.life.shop.api.apis;

import com.nbsaas.life.shop.api.domain.request.ShopReviewDataRequest;
import com.nbsaas.life.shop.api.domain.simple.ShopReviewSimple;
import com.nbsaas.life.shop.api.domain.response.ShopReviewResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ShopReviewApi extends BaseApi<ShopReviewResponse, ShopReviewSimple, ShopReviewDataRequest> {


}
