package com.nbsaas.life.shop.api.domain.field;


/**
*   字段映射类
*/
public class ShopCheckInField  {



    public static final String  latitude = "latitude";


    public static final String  comment = "comment";


    public static final String  gps = "gps";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  longitude = "longitude";


    public static final String  lastDate = "lastDate";

}