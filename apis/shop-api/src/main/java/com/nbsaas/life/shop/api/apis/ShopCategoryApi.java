package com.nbsaas.life.shop.api.apis;

import com.nbsaas.life.shop.api.domain.request.ShopCategoryDataRequest;
import com.nbsaas.life.shop.api.domain.simple.ShopCategorySimple;
import com.nbsaas.life.shop.api.domain.response.ShopCategoryResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ShopCategoryApi extends BaseApi<ShopCategoryResponse, ShopCategorySimple, ShopCategoryDataRequest> {


}
