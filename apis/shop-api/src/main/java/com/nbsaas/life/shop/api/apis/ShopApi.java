package com.nbsaas.life.shop.api.apis;

import com.nbsaas.life.shop.api.domain.request.ShopDataRequest;
import com.nbsaas.life.shop.api.domain.simple.ShopSimple;
import com.nbsaas.life.shop.api.domain.response.ShopResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ShopApi extends BaseApi<ShopResponse, ShopSimple, ShopDataRequest> {


}
