package com.nbsaas.life.shop.api.apis;

import com.nbsaas.life.shop.api.domain.request.ShopImageDataRequest;
import com.nbsaas.life.shop.api.domain.simple.ShopImageSimple;
import com.nbsaas.life.shop.api.domain.response.ShopImageResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ShopImageApi extends BaseApi<ShopImageResponse, ShopImageSimple, ShopImageDataRequest> {


}
