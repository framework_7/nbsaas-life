package com.nbsaas.life.shop.api.domain.field;


/**
*   字段映射类
*/
public class ShopField  {



    public static final String  discountNum = "discountNum";


    public static final String  distance = "distance";


    public static final String  city = "city";


    public static final String  facilityRating = "facilityRating";


    public static final String  latitude = "latitude";


    public static final String  grouponNum = "grouponNum";


    public static final String  poi = "poi";


    public static final String  photos = "photos";


    public static final String  environmentRating = "environmentRating";


    public static final String  province = "province";


    public static final String  price = "price";


    public static final String  state = "state";


    public static final String  id = "id";


    public static final String  longitude = "longitude";


    public static final String  tasteRating = "tasteRating";


    public static final String  lastDate = "lastDate";


    public static final String  hygieneRating = "hygieneRating";


    public static final String  area = "area";


    public static final String  image = "image";


    public static final String  website = "website";


    public static final String  address = "address";


    public static final String  overallRating = "overallRating";


    public static final String  imageNum = "imageNum";


    public static final String  serviceRating = "serviceRating";


    public static final String  shopHours = "shopHours";


    public static final String  addDate = "addDate";


    public static final String  favoriteNum = "favoriteNum";


    public static final String  commentNum = "commentNum";


    public static final String  phone = "phone";


    public static final String  name = "name";


    public static final String  detailUrl = "detailUrl";


    public static final String  technologyRating = "technologyRating";


    public static final String  checkinNum = "checkinNum";


    public static final String  user = "user";

}