package com.nbsaas.life.shop.api.apis;

import com.nbsaas.life.shop.api.domain.request.ShopTagDataRequest;
import com.nbsaas.life.shop.api.domain.simple.ShopTagSimple;
import com.nbsaas.life.shop.api.domain.response.ShopTagResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ShopTagApi extends BaseApi<ShopTagResponse, ShopTagSimple, ShopTagDataRequest> {


}
