package com.nbsaas.life.shop.api.domain.field;


/**
*   字段映射类
*/
public class ShopCategoryField  {



    public static final String  code = "code";


    public static final String  cname = "cname";


    public static final String  icon = "icon";


    public static final String  py = "py";


    public static final String  addDate = "addDate";


    public static final String  path = "path";


    public static final String  pinyin = "pinyin";


    public static final String  depth = "depth";


    public static final String  name = "name";


    public static final String  ids = "ids";


    public static final String  sortNum = "sortNum";


    public static final String  id = "id";


    public static final String  lft = "lft";


    public static final String  nums = "nums";


    public static final String  rgt = "rgt";


    public static final String  lastDate = "lastDate";

}