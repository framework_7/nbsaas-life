package com.nbsaas.life.shop.data.repository;

import com.nbsaas.life.shop.data.entity.ShopTag;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ShopTagRepository  extends  JpaRepositoryImplementation<ShopTag, Serializable>{

}