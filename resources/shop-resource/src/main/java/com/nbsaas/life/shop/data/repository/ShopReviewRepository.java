package com.nbsaas.life.shop.data.repository;

import com.nbsaas.life.shop.data.entity.ShopReview;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ShopReviewRepository  extends  JpaRepositoryImplementation<ShopReview, Serializable>{

}