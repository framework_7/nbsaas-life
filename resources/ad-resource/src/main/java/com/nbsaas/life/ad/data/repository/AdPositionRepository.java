package com.nbsaas.life.ad.data.repository;

import com.nbsaas.life.ad.data.entity.AdPosition;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface AdPositionRepository  extends  JpaRepositoryImplementation<AdPosition, Serializable>{

}