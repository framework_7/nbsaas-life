package com.nbsaas.life.area.data.repository;

import com.nbsaas.life.area.data.entity.AreaHot;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface AreaHotRepository  extends  JpaRepositoryImplementation<AreaHot, Serializable>{

}