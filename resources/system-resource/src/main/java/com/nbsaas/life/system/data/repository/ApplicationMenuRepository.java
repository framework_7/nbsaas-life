package com.nbsaas.life.system.data.repository;

import com.nbsaas.life.system.data.entity.ApplicationMenu;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ApplicationMenuRepository  extends  JpaRepositoryImplementation<ApplicationMenu, Serializable>{

}