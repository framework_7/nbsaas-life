package com.nbsaas.life.system.data.repository;

import com.nbsaas.life.system.data.entity.RoleMenu;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface RoleMenuRepository  extends  JpaRepositoryImplementation<RoleMenu, Serializable>{

}